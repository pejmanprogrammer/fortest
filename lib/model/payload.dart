

import 'dart:convert';

import 'package:coinhash_test/model/statuscode.dart';

import 'dtum.dart';

class Payload {
  Payload({
    this.status,
    this.data,
  });

  Status status;
  List<Datum> data;

  factory Payload.fromJson(Map<String, dynamic> json) => Payload(
    status: Status.fromJson(json["status"]),
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status.toJson(),
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}




Payload payloadFromJson(String str) => Payload.fromJson(json.decode(str));

String payloadToJson(Payload data) => json.encode(data.toJson());