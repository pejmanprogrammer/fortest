

import 'package:coinhash_test/model/quote.dart';
import 'package:coinhash_test/utils/enums.dart';

import 'platform.dart';

class Datum {
  Datum({
    this.id,
    this.name,
    this.symbol,
    this.slug,
    this.numMarketPairs,
    this.dateAdded,
    this.tags,
    this.maxSupply,
    this.circulatingSupply,
    this.totalSupply,
    this.platform,
    this.cmcRank,
    this.lastUpdated,
    this.quote,
  });

  int id;
  String name;
  String symbol;
  String slug;
  int numMarketPairs;
  DateTime dateAdded;
  List<Tag> tags;
  double maxSupply;
  double circulatingSupply;
  double totalSupply;
  Platform platform;
  int cmcRank;
  DateTime lastUpdated;
  Quote quote;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    name: json["name"],
    symbol: json["symbol"],
    slug: json["slug"],
    numMarketPairs: json["num_market_pairs"],
    dateAdded: DateTime.parse(json["date_added"]),
    tags: List<Tag>.from(json["tags"].map((x) => tagValues.map[x])),
    maxSupply:
    json["max_supply"] == null ? null : json["max_supply"].toDouble(),
    circulatingSupply: json["circulating_supply"] == null
        ? null
        : json["circulating_supply"].toDouble(),
    totalSupply: json["total_supply"] == null
        ? null
        : json["total_supply"].toDouble(),
    platform: json["platform"] == null
        ? null
        : Platform.fromJson(json["platform"]),
    cmcRank: json["cmc_rank"],
    lastUpdated: DateTime.parse(json["last_updated"]),
    quote: Quote.fromJson(json["quote"]),
  );




  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "symbol": symbol,
    "slug": slug,
    "num_market_pairs": numMarketPairs,
    "date_added": dateAdded.toIso8601String(),
    "tags": List<dynamic>.from(tags.map((x) => tagValues.reverse[x])),
    "max_supply": maxSupply == null ? null : maxSupply,
    "circulating_supply":
    circulatingSupply == null ? null : circulatingSupply,
    "total_supply": totalSupply == null ? null : totalSupply,
    "platform": platform == null ? null : platform.toJson(),
    "cmc_rank": cmcRank,
    "last_updated": lastUpdated.toIso8601String(),
    "quote": quote.toJson(),
  };


}


final tagValues = EnumValues({"mineable": Tag.MINEABLE});