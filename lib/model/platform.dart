

import 'package:coinhash_test/utils/enums.dart';

class Platform {
  Platform({
    this.id,
    this.name,
    this.symbol,
    this.slug,
    this.tokenAddress,
  });

  int id;
  Name name;
  Symbol symbol;
  Slug slug;
  String tokenAddress;

  factory Platform.fromJson(Map<String, dynamic> json) => Platform(
    id: json["id"],
    name: nameValues.map[json["name"]],
    symbol: symbolValues.map[json["symbol"]],
    slug: slugValues.map[json["slug"]],
    tokenAddress: json["token_address"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": nameValues.reverse[name],
    "symbol": symbolValues.reverse[symbol],
    "slug": slugValues.reverse[slug],
    "token_address": tokenAddress,
  };
}