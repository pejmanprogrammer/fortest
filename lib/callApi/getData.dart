


import 'package:flutter/cupertino.dart';
import 'package:coinhash_test/const/string.dart';
import 'package:coinhash_test/model/payload.dart';
import 'package:http/http.dart' as http ;

Future<Payload> getData({@required String url}) async {
  var response = await http.get(
      url,
      headers: {
        'X-CMC_PRO_API_KEY': KeyApi,
        "Accept": "application/json",
      });

  if (response.statusCode == 200) {
    return payloadFromJson(response.body);
  }
}