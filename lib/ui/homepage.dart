

import 'package:coinhash_test/const/string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinhash_test/const/colors.dart';
import 'package:coinhash_test/controller/homepage.dart';
import 'package:coinhash_test/model/payload.dart';
import 'package:coinhash_test/ui/payloadrow.dart';
import 'package:coinhash_test/utils/enums.dart';
import 'package:http/http.dart' as http;





class HomePage extends GetView<HomePageController>{

  HomePageController controller = Get.put(HomePageController());



  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: GetBuilder<HomePageController>(
          builder: (controller)
          {
             if(controller.stateData == StateData.Loading)
               return Container(color: backColor,child: Center(child: CircularProgressIndicator(backgroundColor: Colors.yellow,)));
             else if(controller.stateData == StateData.NotConnected)
               {
                 return Container(height : double.infinity , width : double.infinity , color : backColor , child : Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: [

                     IconButton(icon: Icon(Icons.refresh , color: Colors.white), onPressed: pressedRefresh),
                     SizedBox(height: 10,),
                     Text(NotConnected_Message , style: TextStyle(color: Colors.white),),

                   ],
                 ),);
               }
             else
               {
                 return Column(
                   children: <Widget>[

                     Expanded(
                       child: new Container(
                         color: new Color(0xFF736AB7),
                         child: new CustomScrollView(
                           scrollDirection: Axis.vertical,
                           shrinkWrap: false,
                           slivers: <Widget>[
                             new SliverPadding(
                               padding: const EdgeInsets.symmetric(vertical: 24.0),
                               sliver: new SliverList(
                                 delegate: new SliverChildBuilderDelegate(
                                       (context, index) => new PayloadRow(index: index),
                                   childCount: controller.payload.data?.length ?? 0,
                                 ),
                               ),
                             ),
                           ],
                         ),
                       ),
                     )


                   ],
                 );
               }

          },
        ));
  }

  void pressedRefresh() {

    controller.checkNetWork();

  }
}




