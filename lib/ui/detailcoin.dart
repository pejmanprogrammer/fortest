
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinhash_test/const/colors.dart';
import 'package:coinhash_test/const/images.dart';
import 'package:coinhash_test/model/dtum.dart';


class DetailCoinScreen extends StatelessWidget {

  Datum datum ;
  DetailCoinScreen({@required this.datum});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: backColor ,
        body: Stack(

          children: [

            Padding(
              padding: const EdgeInsets.only(top: 65 , left: 8 , right: 8 , bottom: 40),
              child: SizedBox(
                width: double.infinity,
                child: Card(
                  elevation: 15,
                  color: Colors.white ,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    children: [

                      SizedBox(height: 80,),
                      itemDetail(name: "Name"  , des: datum?.name ?? "" ),
                      itemDetail(name: "Price"  , des: datum?.quote?.usd?.price?.toString() ?? ""),
                      itemDetail(name: "lastUpdated"  , des: datum?.lastUpdated?.toString() ?? ""),
                      itemDetail(name: "percentChange1H"  , des: datum?.quote?.usd?.percentChange1H?.toString() ?? ""),
                      itemDetail(name: "percentChange24H"  , des: datum?.quote?.usd?.percentChange24H?.toString() ?? ""),
                      itemDetail(name: "percentChange7D"  , des: datum?.quote?.usd?.percentChange7D?.toString() ?? ""),
                      itemDetail(name: "totalSupply"  , des: datum?.totalSupply?.toString() ?? ""),
                      itemDetail(name: "cmcRank"  , des: datum?.cmcRank?.toString() ?? ""),




                    ],
                  ),
                ),
              ),
            ),

            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Image(
                  image: new AssetImage(ImgCoin),
                  height: 92.0,
                  width: 92.0,
                ),
              ),
            ),

          ],
        )
      ),
    );

  }


  Widget itemDetail({@required String name ,@required  String des} )
  {

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12 , horizontal: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [

          Text(name , style: TextStyle(color: Colors.black , fontSize: 16 , fontWeight: FontWeight.w700),),
          Text(des , style: TextStyle(color: Colors.grey[500]  , fontSize: 14 ),),


        ],
      ),
    );
  }



}
