


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinhash_test/const/images.dart';
import 'package:coinhash_test/controller/homepage.dart';
import 'package:coinhash_test/utils/createFromat.dart';

import 'detailcoin.dart';




class PayloadRow extends StatelessWidget {

  int index ;
  PayloadRow({@required this.index});

  HomePageController controller = Get.find();


  @override
  Widget build(BuildContext context) {

    String percent = createFormat(controller.payload.data[index].quote?.usd?.price??0);



    Widget PayloadThumbnail = new Container(
      margin: new EdgeInsets.symmetric(
          vertical: 16.0
      ),
      alignment: FractionalOffset.centerLeft,
      child: Image(
        image: AssetImage(ImgCoin),
        height: 92.0,
        width: 92.0,
      ),
    );

    final baseTextStyle = const TextStyle(
        fontFamily: 'Poppins'
    );
    final regularTextStyle = baseTextStyle.copyWith(
        color: const Color(0xffb6b2df),
        fontSize: 9.0,
        fontWeight: FontWeight.w400
    );
    final subHeaderTextStyle = regularTextStyle.copyWith(
        fontSize: 15.0
    );
    final headerTextStyle = baseTextStyle.copyWith(
        color: Colors.white,
        fontSize: 18.0,
        fontWeight: FontWeight.w600
    );




    final PayloadCardContent = new Container(
      margin: new EdgeInsets.fromLTRB(76.0, 16.0, 16.0, 16.0),
      constraints: new BoxConstraints.expand(),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(height: 4.0),
          new Text(controller.payload.data[index].name, style: headerTextStyle),
          new Container(height: 10.0),
          new Text(percent ,  style: subHeaderTextStyle),
          new Container(
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              height: 2.0,
              width: 18.0,
              color: new Color(0xff00c6ff)
          ),

        ],
      ),
    );


    final PayloadCard = new Container(
      child: PayloadCardContent,
      height: 124.0,
      margin: new EdgeInsets.only(left: 46.0),
      decoration: new BoxDecoration(
        color: new Color(0xFF333366),
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: new Offset(0.0, 10.0),
          ),
        ],
      ),
    );


    return new Container(
        height: 120.0,
        margin: const EdgeInsets.symmetric(
          vertical: 16.0,
          horizontal: 24.0,
        ),
        child: new Stack(
          children: <Widget>[
            InkWell(
                onTap: navigateToDetail,
                child: PayloadCard
            ),
            PayloadThumbnail,
          ],
        )
    );
  }

  navigateToDetail() {

    Get.to(DetailCoinScreen(datum: controller.payload.data[index],));

  }
}